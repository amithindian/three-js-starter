import {Scene,
        PerspectiveCamera,
        WebGLRenderer,
        SphereGeometry,
        MeshLambertMaterial,
        Mesh,
        DirectionalLight,
        SmoothShading,
        TextureLoader} from 'three';
import {Window} from './globals';
import Assets from './assets';

// set up basic needs
let scene = new Scene();
let camera = new PerspectiveCamera(75, Window.innerWidth / Window.innerHeight, 0.1, 1000);
let renderer = new WebGLRenderer({antialias: true});
let textureLoader = new TextureLoader();

// set up the lights
let blueLight = new DirectionalLight('white', 1);
let greenLight = new DirectionalLight('white', 1);
let redLight = new DirectionalLight('white', 1);

blueLight.position.set(-20, 10, 0);
greenLight.position.set(20, 10, 0);
redLight.position.set(0, -10, 0);

// create the football
let texture = textureLoader.load(Assets.footBall);
let material = new MeshLambertMaterial({ map: texture, shading: SmoothShading });
let geometry = new SphereGeometry(1, 360, 360);
let cube = new Mesh(geometry, material);

// add webgl renderer to dom
renderer.setSize(Window.innerWidth, Window.innerHeight);
Window.onresize = () => {
  camera.aspect = Window.innerWidth / Window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(Window.innerWidth, Window.innerHeight);
};
Window.document.body.appendChild(renderer.domElement);
// scene config
scene.add(cube);
scene.add(blueLight);
scene.add(greenLight);
scene.add(redLight);
camera.position.z = 3;

let render = () => {
  cube.rotation.x = cube.rotation.x + 0.01;
  cube.rotation.y = cube.rotation.y + 0.01;
  camera.position.z = camera.position.z + 0.01;

  Window.requestAnimationFrame(render);
  renderer.render(scene, camera);
};

render();

// add index.html for production build
require('file?name=[name].[ext]!../index.html');
